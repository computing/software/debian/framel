/*-----------------------------------------------------------------------------*/
/* FrFileMerge.c  by E. Pacaud                                                 */
/*                                                                             */
/* This application merges two frame files into one file, filling the holes    */
/* in the first one with the frames present in the second one.                 */
/* Usage: FrFileMerge input_file1 input_file2 output_prefix                    */
/*-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "FrameL.h"

#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))

extern int snprintf (char *__restrict __s, size_t __maxlen,
                     __const char *__restrict __format, ...)
     __THROW __attribute__ ((__format__ (__printf__, 3, 4)));

int 
main (int argc, char **argv)
{
	FrFile *input_file1, *input_file2;
	FrFile *output_file;
	FrameH *frame;
	char *input_filename1, *input_filename2, *prefix;
	char *output_filename;
	double start, stop, dt = -1.0, time;
	unsigned int frame_count = 0;
	size_t size;

	if (argc != 4) {
		printf ("Wrong number of parameters\n");
		printf ("Usage: FrFileMerge input_file1 input_file2 output_prefix\n");
		return 1;
	}

	input_filename1 = argv[1];
	input_filename2 = argv[2];
	prefix = argv[3];

	input_file1 = FrFileINew (input_filename1);
	if (input_file1 == NULL) {
		printf ("Can't open input file %s\n", input_filename1);
		goto INTPUT_FILE1_FAILED;
	}
	input_file1->compress = -1;

	input_file2 = FrFileINew (input_filename2);
	if (input_file2 == NULL) {
		printf ("Can't open input file %s\n", input_filename2);
		goto INTPUT_FILE2_FAILED;
	}
	input_file2->compress = -1;

	start = MIN (FrFileITStart (input_file1), FrFileITStart (input_file2));
	stop = MAX (FrFileITEnd (input_file1), FrFileITEnd (input_file2));

	size = strlen (prefix) + 256;
	output_filename = malloc (size);
	snprintf (output_filename, size, "%s-%g-%g.gwf", prefix, start, stop - start);

	output_file = FrFileONew (output_filename, -1);
	if (output_file == NULL) {
		printf ("Can't create output file %s\n", output_filename);
		goto OUTPUT_FAILED;
	}

	time = start;
	while (time <= stop) {
		frame = FrameReadT (input_file1, time);
		if (frame == NULL)
			frame = FrameReadT (input_file2, time);
		if (frame == NULL) {
			if (dt < 0.0) {
				printf ("No frame found\n");
				goto FAILED;
			}
			printf ("Frame missing at %g\n", time);
			time += dt;
		}
		dt = frame->dt;
		FrameWrite (frame, output_file); 
		frame_count++;
	}

	printf ("Processed %d frame(s)\n", frame_count);

FAILED:
	FrFileOEnd (output_file);

OUTPUT_FAILED:
	FrFileITEnd (input_file2);

INTPUT_FILE2_FAILED:
	FrFileITEnd (input_file1);

INTPUT_FILE1_FAILED:

	return 0;
}
